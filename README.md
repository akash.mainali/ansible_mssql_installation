Playbook:  windows_sql_install.yml 
Description: This playbook is responsible to install single node MSSQL server. It is targeting the machine created by the role “windows_sql_provisioning” and installing MSSQL in it.
Roles Used: windows_mssql_installation

###############################################################################################################################################################

Role: windows_mssql_installation
Description: This role is responsible for installation of MSSQL using a customization file.
Tasks in the role:
    ● First this role will use ansible template module and create the configuration file and save the configuration file in “C:\Windows\tmp\ConfigurationFile ini.
    ● Secondly this role will run powershell command to install MSSQL using the configuration file provided.
    ● Then the ansible role will delete the configuration file after the installation has been success.
    ● After MsSql has been installed, the playbook will install SQL Server Manager
    
Variables used by the role: (Can be passed through extra vars)
    ● sa_password: Password for MsSql


Hardware and software requirement to install MsSql 2019 in Windows 2019:
https://docs.microsoft.com/en-us/sql/sql-server/install/hardware-and-software-requirements-for-installing-sql-server?view=sql-server-ver15
